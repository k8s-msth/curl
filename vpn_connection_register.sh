#!/bin/sh

VPNSERVICE="${HOSTNAME%%-*}.vpn:3128"
echo "Registering VPN connection $VPNSERVICE"
while :
do
	curl http://requesthub.default:3000/vpn_sessions/register -d "address=$VPNSERVICE"
	sleep 300
done
